# Copyright 2022 Studio73 - Miguel Gandia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Website Sale Barcode Search",
    "category": "E-Commerce",
    "summary": "It improve website product search adding search by barcode",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "depends": ["website_sale"],
    "author": "Studio73, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "installable": True,
}
