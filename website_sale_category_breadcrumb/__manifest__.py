# ©  2021 Radovan Skolnik <radovan@skolnik.info>
{
    "name": "eCommerce Product Category Breadcrumb",
    "category": "Website",
    "summary": "Displays Product Category Breadcrumb(s) in eCommerce",
    "version": "2.0.1.0.1",
    "author": "Radovan Skolnik, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "depends": ["website_sale"],
    "data": ["views/templates.xml"],
    "installable": True,
    "development_status": "Beta",
    "maintainers": ["Rad0van"],
}
