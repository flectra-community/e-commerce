{
    "name": "Website Sale Filter Product Brand",
    "author": "Advitus MB, Ooops, Cetmix, Odoo Community Association (OCA)",
    "version": "2.0.1.0.2",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "category": "Website/Website",
    "depends": ["product_brand", "website_sale"],
    "demo": [
        "demo/product_brand_demo.xml",
        "demo/product_product_demo.xml",
    ],
    "data": [
        "views/assets.xml",
        "views/templates.xml",
    ],
    "license": "AGPL-3",
    "installable": True,
    "auto_install": False,
    "application": False,
}
