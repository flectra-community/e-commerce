# Copyright 2021 Tecnativa - Carlos Roca
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
{
    "name": "Website Sale Wishlist Keep",
    "category": "Website",
    "summary": 'Allows to add products to my cart but keep it in my wishlist"',
    "development_status": "Beta",
    "version": "2.0.1.0.0",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "license": "LGPL-3",
    "depends": ["website_sale_wishlist"],
    "data": ["templates/assets.xml", "views/website_sale_wishlist_template.xml"],
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "installable": True,
}
