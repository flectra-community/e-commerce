# Flectra Community / e-commerce

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[website_sale_product_attribute_filter_visibility](website_sale_product_attribute_filter_visibility/) | 2.0.1.0.0| Allow hide any attributes in shop attributes filter
[website_sale_product_item_cart_custom_qty](website_sale_product_item_cart_custom_qty/) | 2.0.1.1.0| Allows to add to cart from product items a custom quantity.
[website_sale_wishlist_keep](website_sale_wishlist_keep/) | 2.0.1.0.0| Allows to add products to my cart but keep it in my wishlist"
[website_sale_payment_term_acquirer](website_sale_payment_term_acquirer/) | 2.0.1.0.1| eCommerce Payment Term Acquirer
[website_sale_order_type](website_sale_order_type/) | 2.0.1.0.0| This module allows sale_order_type to work with website_sale.
[product_variant_multi_link](product_variant_multi_link/) | 2.0.1.1.0| Product Multi Links (Variant)
[website_sale_stock_provisioning_date](website_sale_stock_provisioning_date/) | 2.0.1.0.0| Display provisioning date for a product in shop online
[website_sale_attribute_filter_multiselect](website_sale_attribute_filter_multiselect/) | 2.0.1.0.0| Add multiselect display type for product and new filter for it
[website_sale_cart_expire](website_sale_cart_expire/) | 2.0.1.0.2| Expire abandoned carts
[website_sale_product_assortment](website_sale_product_assortment/) | 2.0.1.0.2| Use product assortments to display products available on e-commerce.
[website_sale_attribute_filter_form_submit](website_sale_attribute_filter_form_submit/) | 2.0.1.0.0| Allow to apply manually the filters on the e-commerce
[website_sale_product_brand](website_sale_product_brand/) | 2.0.1.1.0| Product Brand Filtering in Website
[product_template_multi_link](product_template_multi_link/) | 2.0.1.3.0| Product Multi Links (Template)
[website_sale_require_login](website_sale_require_login/) | 2.0.1.0.0| Force users to login for buying
[website_sale_checkout_country_vat](website_sale_checkout_country_vat/) | 2.0.1.0.1| Autocomplete VAT in checkout process
[website_sale_b2x_alt_price](website_sale_b2x_alt_price/) | 2.0.1.0.0| Display prices with(out) taxes in eCommerce, complementing normal mode
[website_sale_product_description](website_sale_product_description/) | 2.0.1.1.0| Shows custom e-Commerce description for products
[website_sale_stock_available](website_sale_stock_available/) | 2.0.1.0.0| Display 'Available to promise' in shop online instead 'Quantity On Hand'
[website_sale_show_company_data](website_sale_show_company_data/) | 2.0.1.0.0| Show commercial partner data if any
[website_sale_filter_product_brand](website_sale_filter_product_brand/) | 2.0.1.0.2| Website Sale Filter Product Brand
[website_sale_suggest_create_account](website_sale_suggest_create_account/) | 2.0.1.0.1| Suggest users to create an account when buying in the website
[website_sale_require_legal](website_sale_require_legal/) | 2.0.1.0.0| Force the user to accept legal tems to buy in the web shop
[website_sale_product_detail_attribute_value_image](website_sale_product_detail_attribute_value_image/) | 2.0.1.0.0| Display attributes values images in shop product detail
[website_sale_product_minimal_price](website_sale_product_minimal_price/) | 2.0.1.0.0| Display minimal price for products that has variants
[website_sale_charge_payment_fee](website_sale_charge_payment_fee/) | 2.0.1.0.0| Payment fee charged to customer
[product_template_multi_link_date_span](product_template_multi_link_date_span/) | 2.0.1.1.0| Add an optional date span for when a link is active.
[website_sale_ajaxify_cart](website_sale_ajaxify_cart/) | 2.0.1.0.1| eCommerce Ajaxify cartr
[website_sale_product_attribute_filter_category](website_sale_product_attribute_filter_category/) | 2.0.1.0.0| Allow group attributes in shop by categories
[website_sale_product_attachment](website_sale_product_attachment/) | 2.0.1.0.0| Let visitors download attachments from a product page
[website_sale_product_detail_attribute_image](website_sale_product_detail_attribute_image/) | 2.0.1.0.0| Display attributes images in shop product detail
[website_sale_barcode_search](website_sale_barcode_search/) | 2.0.1.0.0| It improve website product search adding search by barcode
[website_sale_product_name_fixed_height](website_sale_product_name_fixed_height/) | 2.0.1.0.0| This module lets user set for each website how many lines                of description to display in /shop page
[website_sale_category_breadcrumb](website_sale_category_breadcrumb/) | 2.0.1.0.1| Displays Product Category Breadcrumb(s) in eCommerce
[website_sale_product_attribute_value_filter_existing](website_sale_product_attribute_value_filter_existing/) | 2.0.1.0.0| Allow hide attributes values not used in variants
[website_sale_hide_empty_category](website_sale_hide_empty_category/) | 2.0.1.0.0| Hide any Product Categories that are empty
[website_sale_hide_price](website_sale_hide_price/) | 2.0.1.1.0| Hide product prices on the shop
[website_sale_stock_list_preview](website_sale_stock_list_preview/) | 2.0.1.0.0| Show the stock of products on the product previews
[website_sale_invoice_address](website_sale_invoice_address/) | 2.0.1.0.1| Set e-Commerce sale orders invoice address as in backend
[website_sale_attribute_filter_price](website_sale_attribute_filter_price/) | 2.0.1.0.1| A price filter for website sale
[website_sale_checkout_skip_payment](website_sale_checkout_skip_payment/) | 2.0.1.2.0| Skip payment for logged users in checkout process
[website_sale_delivery_group](website_sale_delivery_group/) | 2.0.1.0.1| Provides a way to group shipping methods
[website_sale_tax_toggle](website_sale_tax_toggle/) | 2.0.1.0.0| Allow display price in Shop with or without taxes
[website_sale_infinite_scroll](website_sale_infinite_scroll/) | 2.0.1.0.2| eCommerce Infinite Scroll


