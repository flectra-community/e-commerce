# Copyright 2020 Tecnativa - Carlos Roca
# Copyright 2020 Tecnativa - Sergio Teruel
# Copyright 2020 Tecnativa - Carlos Dauden
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Website Sale Stock List Preview",
    "summary": "Show the stock of products on the product previews",
    "version": "2.0.1.0.0",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "depends": ["website_sale_stock"],
    "data": ["views/assets.xml", "views/templates.xml"],
}
