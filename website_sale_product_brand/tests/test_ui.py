# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import flectra.tests


@flectra.tests.tagged("post_install", "-at_install")
class UICase(flectra.tests.HttpCase):
    def test_ui_website(self):
        """Test frontend tour."""
        self.start_tour("/shop", "website_sale_product_brand", login="portal")
