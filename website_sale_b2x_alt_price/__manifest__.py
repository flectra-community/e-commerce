# Copyright 2020 Jairo Llopis - Tecnativa
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
{
    "name": "Alternative (un)taxed prices display on eCommerce",
    "summary": "Display prices with(out) taxes in eCommerce, complementing normal mode",
    "version": "2.0.1.0.0",
    "development_status": "Beta",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "maintainers": ["Yajo"],
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": ["website_sale"],
    "data": ["templates/assets.xml", "templates/product.xml"],
}
