# Copyright 2021 Tecnativa - Carlos Roca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Website Sale Product Cart Quantity",
    "summary": "Allows to add to cart from product items a custom quantity.",
    "version": "2.0.1.1.0",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["website_sale"],
    "data": ["views/assets.xml", "views/templates.xml"],
    "maintainers": ["CarlosRoca13"],
}
