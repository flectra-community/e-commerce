# Copyright 2020 Tecnativa - David Vidal
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Website Sale Invoice Address",
    "summary": "Set e-Commerce sale orders invoice address as in backend",
    "version": "2.0.1.0.1",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["website_sale"],
    "data": ["views/assets.xml"],
}
