{
    "name": "eCommerce Ajaxify cart",
    "summary": "eCommerce Ajaxify cartr",
    "author": "Ooops, Cetmix, Odoo Community Association (OCA)",
    "version": "2.0.1.0.1",
    "category": "Website/Website",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "maintainers": ["geomer198", "CetmixGitDrone"],
    "license": "AGPL-3",
    "depends": ["website_sale"],
    "data": [
        "views/assets.xml",
        "views/templates.xml",
    ],
    "demo": [],
    "installable": True,
}
