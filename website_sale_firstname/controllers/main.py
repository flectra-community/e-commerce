# Copyright 2018 Denis Mudarisov (IT-Projects LLC)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from flectra.addons.website_sale.controllers.main import WebsiteSale


class WebsiteSaleExtended(WebsiteSale):
    def _get_mandatory_billing_fields(self):
        result = [
            fname for fname in super()._get_mandatory_billing_fields()
            if fname != 'name'
        ]
        result += ['firstname', 'lastname']
        return result

    def _get_mandatory_shipping_fields(self):
        result = [
            fname for fname in super()._get_mandatory_shipping_fields()
            if fname != 'name'
        ]
        result += ['firstname', 'lastname']
        return result
