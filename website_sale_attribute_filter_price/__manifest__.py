# Copyright 2020 Tecnativa - Alexandre Díaz
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
{
    "name": "Website Sale Attribute Filter Price",
    "category": "Website",
    "summary": "A price filter for website sale",
    "version": "2.0.1.0.1",
    "license": "LGPL-3",
    "depends": ["website_sale"],
    "data": ["templates/assets.xml", "templates/shop.xml"],
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "installable": True,
    "maintainers": ["Tardo"],
}
