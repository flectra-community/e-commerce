{
    "name": "eCommerce Payment Term Acquirer",
    "summary": "eCommerce Payment Term Acquirer",
    "author": "Ooops, Cetmix, Odoo Community Association (OCA)",
    "version": "2.0.1.0.1",
    "category": "Website",
    "website": "https://gitlab.com/flectra-community/e-commerce",
    "maintainers": ["geomer198", "CetmixGitDrone"],
    "license": "AGPL-3",
    "depends": ["website_sale"],
    "data": [
        "views/payment_acquirer_views.xml",
        "views/payment_templates.xml",
    ],
    "demo": ["demo/demo.xml"],
    "installable": True,
}
